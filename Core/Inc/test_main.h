#include "stdbool.h"
#include "stdint.h"

#include "GPIO.h"
#include "calc.h"
#include "screen.h"

#define NO_ERROR 0
#define ERROR_CODE_BASE 1
#define ERROR_CODE_PLUS 2
#define ERROR_CODE_MINUS 3
#define ERROR_CODE_MULTIPLY 4
#define ERROR_CODE_DIVIDE 5
#define ERROR_CODE_POWER 6

#define ERROR_CODE_PARENTHESIS 6
#define ERROR_CODE_PRIORITY 7

#define ERROR_CODE_COMPLEXE 8

uint8_t tests_algo();
uint8_t tests_hardware();
