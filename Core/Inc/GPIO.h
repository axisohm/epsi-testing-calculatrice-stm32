#include "stdbool.h"
#include "stdint.h"
#include "stm32f4xx_hal.h"

// Joystick :
#define J1_V_PORT GPIOB
#define J1_H_PORT GPIOB
#define J1_SEL_PORT GPIOE

#define J1_V_PIN GPIO_PIN_0
#define J1_H_PIN GPIO_PIN_1
#define J1_SEL_PIN GPIO_PIN_7

#define STICK_NORMAL 0
#define STICK_PUSHED 1
#define STICK_PULLED 2
#define STICK_ERROR 3 // Le CAN n'as pas répondu.

#define STICK_ADC_MAXVALUE 4095 // = 3,3V
#define STICK_ADC_UPPEREDGE 3071 // 0b101111111111
#define STICK_ADC_LOWEREDGE 1023 // 0b011111111111

#define FLAG_FALSE 0
#define FLAG_TRUE 1

uint8_t getStickStatus_H();
uint8_t getStickStatus_V();
uint8_t check_resetBP();
void resetBP();
