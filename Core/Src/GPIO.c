#include "GPIO.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern uint8_t cursorX;
extern uint8_t cursorY;

uint16_t conversionResultsV;
uint16_t conversionResultsH;
uint8_t bp_flag;

void checkStickPosition() {
	uint8_t increment = 0;
	while(getStickStatus_V() == STICK_PUSHED) {
		if((increment % 1000 == 0) && (cursorY > 0)) {
			cursorY--;
			updateSelectionLocation();
		}
		increment++;
		HAL_Delay(1);
	}
	increment = 0;
	while(getStickStatus_V() == STICK_PULLED) {
		if((increment % 1000 == 0) && (cursorY < 4)) {
			cursorY++;
			updateSelectionLocation();
		}
		increment++;
		HAL_Delay(1);
	}
	increment = 0;
	while(getStickStatus_H() == STICK_PULLED) {
		if((increment % 1000 == 0) && (cursorX < 3)) {
			cursorX++;
			updateSelectionLocation();
		}
		increment++;
		HAL_Delay(1);
	}
	increment = 0;
	while(getStickStatus_H() == STICK_PUSHED) {
		if((increment % 1000 == 0) && (cursorX > 0)) {
			cursorX--;
			updateSelectionLocation();
		}
		increment++;
		HAL_Delay(1);
	}
}

uint8_t getStickStatus_V() {
    HAL_ADC_Start(&hadc1);
	if(HAL_ADC_PollForConversion(&hadc1, 1000000) != HAL_OK)
		return STICK_ERROR;
	conversionResultsV = HAL_ADC_GetValue(&hadc1);
	if(conversionResultsV >= STICK_ADC_UPPEREDGE){
		return STICK_PUSHED;
	}
	else if(conversionResultsV <= STICK_ADC_LOWEREDGE){
		return STICK_PULLED;
	}
	else {
		return STICK_NORMAL;
	}
}

uint8_t getStickStatus_H() {
    HAL_ADC_Start(&hadc2);
	if(HAL_ADC_PollForConversion(&hadc2, 1000000) != HAL_OK)
		return STICK_ERROR;
	conversionResultsH = HAL_ADC_GetValue(&hadc2);
	if(conversionResultsH >= STICK_ADC_UPPEREDGE){
		return STICK_PUSHED;
	}
	else if(conversionResultsH <= STICK_ADC_LOWEREDGE){
		return STICK_PULLED;
	}
	else {
		return STICK_NORMAL;
	}

}


uint8_t check_resetBP() {
	uint8_t result = bp_flag;
	bp_flag = FLAG_FALSE;
	return result;
}

void resetBP() {
	bp_flag = FLAG_FALSE;
}
