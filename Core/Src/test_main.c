#include "test_main.h"
#include "GPIO.h"

uint8_t tests_algo(){
	// TESTS LOGICIEL :
	int32_t test = get_result("100", sizeof("100") - 1);
	if(test != 100)
		return ERROR_CODE_BASE;

	// ADDITIONS :
	test = get_result("1+1", sizeof("1+1") - 1);
	if(test != 2)
		return ERROR_CODE_PLUS;

	test = get_result("20+3", sizeof("20+3") - 1);
	if(test != 23)
		return ERROR_CODE_PLUS;

	test = get_result("100+3+2", sizeof("100+3+2") - 1);
	if(test != 105)
		return ERROR_CODE_PLUS;

	test = get_result("12+25+345+1+2", sizeof("12+25+345+1+2") - 1);
	if(test != 385)
		return ERROR_CODE_PLUS;


	// SOUSTRACTIONS :
	test = get_result("1-1", sizeof("1-1") - 1);
	if(test != 0)
		return ERROR_CODE_MINUS;

	test = get_result("6-4", sizeof("6-4") - 1);
	if(test != 2)
		return ERROR_CODE_MINUS;

	test = get_result("-2--5", sizeof("-2--5") - 1);
	if(test != 3)
		return ERROR_CODE_MINUS;

	test = get_result("3-7", sizeof("6-4") - 1);
	if(test != -4)
		return ERROR_CODE_MINUS;

	// MULTIPLICATIONS :
	test = get_result("1*1", sizeof("1*1") - 1);
	if(test != 1)
		return ERROR_CODE_MULTIPLY;

	test = get_result("2*1", sizeof("2*1") - 1);
	if(test != 2)
		return ERROR_CODE_MULTIPLY;

	test = get_result("1*2", sizeof("1*2") - 1);
	if(test != 2)
		return ERROR_CODE_MULTIPLY;

	test = get_result("6*8", sizeof("6*8") - 1);
	if(test != 48)
		return ERROR_CODE_MULTIPLY;


	// DIVISIONS :
	test = get_result("1/1", sizeof("1/1") - 1);
	if(test != 1)
		return ERROR_CODE_DIVIDE;

	test = get_result("2/1", sizeof("2/1") - 1);
	if(test != 2)
		return ERROR_CODE_DIVIDE;

	test = get_result("2/2", sizeof("2/2") - 1);
	if(test != 1)
		return ERROR_CODE_DIVIDE;

	test = get_result("9/3", sizeof("9/3") - 1);
	if(test != 3)
		return ERROR_CODE_DIVIDE;


	// PUISSANCES :
	test = get_result("1^1", sizeof("1^1") - 1);
	if(test != 1)
		return ERROR_CODE_POWER;

	test = get_result("2^1", sizeof("2^1") - 1);
	if(test != 2)
		return ERROR_CODE_POWER;

	test = get_result("3^3", sizeof("3^3") - 1);
	if(test != 27)
		return ERROR_CODE_POWER;

	test = get_result("1^8", sizeof("1^8") - 1);
	if(test != 1)
		return ERROR_CODE_POWER;

	test = get_result("4^2", sizeof("4^2") - 1);
	if(test != 16)
		return ERROR_CODE_POWER;

	return 0;
}



uint8_t tests_hardware() {
	// TESTS AFFICHAGE POUR VERIF VISUELLE :
	FontDef selectedFont = Font_7x10;
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0xFF0000));
	HAL_Delay(1000);
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x00FF00));
	HAL_Delay(1000);
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x0000FF));
	HAL_Delay(1000);
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0xFFFFFF));
	HAL_Delay(1000);
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_FillRectangle(140, OFFSET_HEIGHT, 20, 20,HEXA_TO_RGB(0xFF0000));
	ST7735_FillRectangle(140, 60 + OFFSET_HEIGHT, 20, 20,HEXA_TO_RGB(0x00FF00));
	ST7735_FillRectangle(0, 60 + OFFSET_HEIGHT, 20, 20,HEXA_TO_RGB(0x0000FF));
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 20, 20,HEXA_TO_RGB(0xFFFFFF));
	ST7735_FillRectangle(70, 30 + OFFSET_HEIGHT, 20, 20,HEXA_TO_RGB(0x808080));
	HAL_Delay(2000);
	/* DEBUG HELP
	// TESTS GPIO :
	while(1){//(getStickStatus_H() != STICK_NORMAL) || (getStickStatus_V() != STICK_NORMAL)) {
		uint8_t H_status = getStickStatus_H();
		uint8_t V_status = getStickStatus_V();
		if(H_status == STICK_PUSHED)
			ST7735_WriteString(0, 20, "H PUSHED", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
		else if(H_status == STICK_PULLED)
			ST7735_WriteString(0, 20, "H PULLED", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
		else
			ST7735_WriteString(0, 20, "H NORMAL", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
		if(V_status == STICK_PUSHED)
			ST7735_WriteString(70, 20, "V PUSHED", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
		else if(V_status == STICK_PULLED)
			ST7735_WriteString(70, 20, "V PULLED", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
		else
			ST7735_WriteString(70, 20, "V NORMAL", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
		HAL_Delay(10); // Attente du stick.
	}
	*/
	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_WriteString(0, OFFSET_HEIGHT, "Centrez le joystick.", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
	while((getStickStatus_H() != STICK_NORMAL) || (getStickStatus_V() != STICK_NORMAL)) HAL_Delay(10); // Attente du stick.

	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_WriteString(0, OFFSET_HEIGHT, "Poussez le joystick a droite.", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
	while((getStickStatus_H() != STICK_PULLED) || (getStickStatus_V() != STICK_NORMAL)) HAL_Delay(10); // Attente du stick.

	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_WriteString(0, OFFSET_HEIGHT, "Poussez le joystick a gauche.", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
	while((getStickStatus_H() != STICK_PUSHED) || (getStickStatus_V() != STICK_NORMAL)) HAL_Delay(10); // Attente du stick.

	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_WriteString(0, OFFSET_HEIGHT, "Montez le joystick.", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
	while((getStickStatus_H() != STICK_NORMAL) || (getStickStatus_V() != STICK_PUSHED)) HAL_Delay(10); // Attente du stick.

	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_WriteString(0, OFFSET_HEIGHT, "Descendez le joystick.", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
	while((getStickStatus_H() != STICK_NORMAL) || (getStickStatus_V() != STICK_PULLED)) HAL_Delay(10); // Attente du stick.

	ST7735_FillRectangle(0, OFFSET_HEIGHT, 160, 80,HEXA_TO_RGB(0x000000));
	ST7735_WriteString(0, OFFSET_HEIGHT, "Pressez le joystick.", selectedFont, HEXA_TO_RGB(FC_KEY_COLOR), HEXA_TO_RGB(BG_KEY_COLOR));
	resetBP();
	while(check_resetBP() != FLAG_TRUE) HAL_Delay(10); // Attente du stick.
	return 0;
}
